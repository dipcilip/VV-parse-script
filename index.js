import puppeteer from "puppeteer";
import fs, { access } from "fs";

async function saveParseData(result, url) {
  let text =
    `${url}\n\n` +
    `name= ${result.name}\n` +
    `heft= ${result.heft}\n` +
    `volume= ${result.volume} оценок\n` +
    `price= ${result.price} ₽\n`;
  if (result.oldPrice) text += `oldPrice= ${result.oldPrice} ₽\n`;

  const times = new Date().getTime();
  const screenshot = `${times}_screenshot.jpg`;

  await fs.promises.writeFile(
    screenshot,
    Buffer.from(result.screenshot, "base64")
  );
  await fs.promises.writeFile(`${times}_productProp.txt`, text, {
    flag: "a",
  });
}

async function runner() {
  const url = process.argv[2];
  if (!url) {
    throw new Error("Не передан URL");
  }
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();
  await page.setViewport({ height: 1366, width: 1500 });
  await page.goto(url);

  const result = await startParse(page);

  result.screenshot = await page.screenshot({
    encoding: "base64",
    fullPage: true,
  });

  await saveParseData(result, url);

  await browser.close();
}
async function startParse(page) {
  let name, heft, price, oldPrice, volume;

  const ratingBtn = await page.waitForSelector('button[data-id*="tabs-id-2"]', {
    timeout: 6000,
  });
  await Promise.all([ratingBtn.hover(), ratingBtn.click()], { timeout: 5000 });

  try {
    name = await page.$eval('h1[class*="list-name"]', (element) =>
      element.innerHTML.trim().replace(/\s*(&nbsp;)\s*/g, " ")
    );
    heft = await page.$eval('div[class*="weight"]', (element) =>
      element.textContent.trim()
    );
    volume = await page.$eval(
      'button[data-id*="id-2"] div[class*="toggler-counter"]',
      (element) => element.innerText.trim().replace(/\s*(&nbsp;)\s*/g, " ")
    );
    price = await page.$eval(
      'span[class*="label"] > span[class*="value"]',
      (element) => element.textContent.trim()
    );
    oldPrice = await page.$('span[class*="last"] > span[class*="value"]', {
      timeout: 5000,
    });
    if (oldPrice) {
      oldPrice = await page.evaluate(
        (element) => element.textContent.trim(),
        oldPrice
      );
      return {
        name,
        heft,
        volume,
        price,
        oldPrice,
      };
    }
  } catch (error) {
    console.error("Ошибка при парсинге:", error);
  }
  return {
    name,
    heft,
    volume,
    price,
  };
}

await runner();
